from django.db import models
from django.contrib.auth.models import User


class ReviewQueue(models.Model):
    url = models.URLField()
    assigned = models.ForeignKey(User, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    finished = models.BooleanField()