from piston.handler import BaseHandler
from django.http import HttpResponse
from django.contrib.auth.models import User
from criticQueue.api.forms import *
from criticQueue.app.models import ReviewQueue


class TeamMemberHandler(BaseHandler):
    allowed_methods = ('GET', 'POST', 'PUT', 'DELETE')
    fields = ('id', 'username', 'first_name', 'last_name', 'email')
    model = User

    def read(self, request):
        if request.user.is_superuser:
            base = User.objects
            return [{"id": q.id, "username": q.username, "first_name": q.first_name, "last_name": q.last_name,
                     "admin": q.is_superuser, "email": q.email} for q in base.all()]
        else:
            return HttpResponse('Unauthorized', status=401)

    def update(self, request):
        if request.user.is_superuser:
            form = UpdateUserForm(request.PUT)
            if form.is_valid():
                return form.save()
            else:
                return HttpResponse(form.errors, status=409)
        else:
            return HttpResponse('Unauthorized', status=401)

    def create(self, request):
        if request.user.is_superuser:
            # try:
            form = CreateUserForm(request.POST)
            if form.is_valid():
                return form.save()
            else:
                return HttpResponse(form.errors, status=409)
        else:
            return HttpResponse('Unauthorized', status=401)

    def delete(self, request):
        if request.user.is_superuser:
            user = User.objects.get(id=int(request.GET.get('id')))
            if user != request.user:
                user.delete()
        else:
            return HttpResponse('Unauthorized', status=401)


class QueueHandler(BaseHandler):
    allowed_methods = ('GET', 'POST', 'PUT', 'DELETE')
    fields = ('id', 'url', 'assigned', 'created', 'finished')
    model = ReviewQueue

    def read(self, request):
        if request.user.is_authenticated():
            base = ReviewQueue.objects
            return base.all().order_by('created')
        else:
            return HttpResponse('Unauthorized', status=401)

    def create(self, request):
        if request.user.is_authenticated():
            form = CreateReviewForm(request.POST)
            if form.is_valid():
                review = {}
                review['url'] = form.cleaned_data['url']
                reviewElement = ReviewQueue(**review)
                reviewElement.save()
                return reviewElement
            else:
                return HttpResponse(form.errors, status=409)
        else:
            return HttpResponse('Unauthorized', status=401)

    def update(self, request):
        if request.user.is_authenticated():
            form = UpdateReviewForm(request.PUT)
            if form.is_valid():
                record = ReviewQueue.objects.get(id=form.cleaned_data['id'])
                if request.GET.get('type') == 'assign':
                    record.assigned = request.user
                elif request.GET.get('type') == 'done':
                    record.finished = True
                else:
                    return HttpResponse('Wrong parameter', status=404)
                record.save()
                return record
            else:
                return HttpResponse(form.errors, status=409)
        else:
            return HttpResponse('Unauthorized', status=401)
