from django.conf.urls.defaults import *
from django.conf import settings
from piston.resource import Resource
from criticQueue.api.handlers import TeamMemberHandler, QueueHandler
#
#
teammemeber_resource = Resource(TeamMemberHandler)
queue_resource = Resource(QueueHandler)

urlpatterns = patterns('',
     url(r'^teammember/$', teammemeber_resource),
     url(r'^queue/$', queue_resource),

)
