from django import forms
from django.contrib.auth.models import User
from criticQueue.app.models import ReviewQueue


class CreateUserForm(forms.Form):
    username = forms.CharField(max_length=30)
    first_name = forms.CharField(required=False, initial='')
    last_name = forms.CharField(required=False, initial='')
    password = forms.CharField(max_length=30, widget=forms.PasswordInput())
    email = forms.EmailField(required=True)

    def clean_username(self):  # check if username dos not exist before
        try:
            User.objects.get(username=self.cleaned_data['username'])  # get user from user model
        except User.DoesNotExist:
            return self.cleaned_data['username']

        raise forms.ValidationError("this user exist already")

    def save(self):  # create new user
        new_user = User.objects.create_user(self.cleaned_data['username'],
                                            self.cleaned_data['email'],
                                            self.cleaned_data['password'])
        new_user.first_name = self.cleaned_data['first_name']
        new_user.last_name = self.cleaned_data['last_name']
        new_user.save()
        return new_user


class UpdateUserForm(forms.Form):
    id = forms.IntegerField(required=True)
    first_name = forms.CharField(required=False, initial='')
    last_name = forms.CharField(required=False, initial='')
    email = forms.EmailField(required=True)

    def save(self):  # create new user
        user = User.objects.get(id=int(self.cleaned_data['id']))
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.save()
        return user


class CreateReviewForm(forms.Form):
    url = forms.URLField(required=True)


class UpdateReviewForm(forms.Form):
    id = forms.IntegerField(required=True)

    def clean_id(self):
        try:
            ReviewQueue.objects.get(id=self.cleaned_data['id'])
            return self.cleaned_data['id']
        except ReviewQueue.DoesNotExist:
            raise forms.ValidationError("review doesn't exists")

