from django.test import TestCase
from django.contrib.auth.models import User
from django.utils import simplejson


class AdminTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('admin', 'admin@world.com', 'admin')
        self.user.is_superuser = True
        self.user.save()

    def tearDown(self):
        self.user.delete()


class TeamMemberTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('teammember', 'teammember@world.com', 'teammember')
        self.user.is_superuser = False
        self.user.save()

    def tearDown(self):
        self.user.delete()


class UsersTest(AdminTests):
    def test_unauthenticated_user(self):
        resp = self.client.get('/api/teammember/')
        self.assertEquals(resp.status_code, 401)

    def test_not_superuser(self):
        self.client.login(username='teammember', password='teammember')
        resp = self.client.get('/api/teammember/')
        self.assertEquals(resp.status_code, 401)

    def test_superuser(self):
        self.client.login(username='admin', password='admin')
        resp = self.client.get('/api/teammember/')
        self.assertEquals(resp.status_code, 200)

    def test_create_user(self):
        self.client.login(username='admin', password='admin')
        resp = self.client.post('/api/teammember/',
                                {'username': 'john', 'first_name': '', 'last_name': '', 'password': 'JohnPass',
                                 'email': 'john@opera.com'})
        self.assertEquals(resp.status_code, 200)
        resp = self.client.get('/api/teammember/')
        users = simplejson.loads(resp.content)
        self.assertEquals(len(users), 2)


class ReviewsTest(TeamMemberTests):
    def test_unauthenticated_user(self):
        resp = self.client.get('/api/queue/')
        self.assertEquals(resp.status_code, 401)

    def test_authenticated_user(self):
        self.client.login(username='teammember', password='teammember')
        resp = self.client.get('/api/queue/')
        self.assertEquals(resp.status_code, 200)

    def test_create_review(self):
        self.client.login(username='teammember', password='teammember')
        url = 'https://critic.oslo.osa/r/43667'
        resp = self.client.post('/api/queue/', {'url': url})
        self.assertEquals(resp.status_code, 200)
        resp = self.client.get('/api/queue/')
        self.assertEquals(resp.status_code, 200)
        reviews = simplejson.loads(resp.content)
        self.assertEquals(len(reviews), 1)
        self.assertEquals(reviews[0].get('url'), url)

