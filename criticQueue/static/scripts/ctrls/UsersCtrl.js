criticQueueApp.controller('UsersCtrl', function ($scope, $http, $modal, $log) {
  $scope.user = {};
  $scope.errors;
  var update = function(){
  $http.get('api/teammember/').success(function (data) {
    $scope.user.users = data || [];
    document.body.classList.remove('loading');
  })
    .error(function (err) {
      $scope.errors = err;
      document.body.classList.remove('loading');
    });
  }
  update();
  $scope.edit = function ($index) {

    var modalInstance = $modal.open({
      templateUrl: 'media/scripts/templates/users/editUser.html',
      controller: 'editUserCtrl',
      size: 'md',
      resolve: {
        user: function () {
          return $scope.user.users[$index];
        }
      }
    });
    var updateUser = function (updatedUser) {
      $http.put('api/teammember/', updatedUser).success(function (data) {
        $scope.user.users[$index] = data;
      }).error(function () {
        document.body.classList.remove('loading');
      });
    }
    modalInstance.result.then(function (updatedUser) {
      updateUser(updatedUser);
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };
  $scope.create = function () {
    var modalInstance = $modal.open({
      templateUrl: 'media/scripts/templates/users/createUser.html',
      controller: 'createUserCtrl',
      size: 'md',
    });
    var createUser = function (newUser) {
      $http.post('api/teammember/', newUser).success(function (data) {
        $scope.user.users.push(data);
      }).error(function (e) {
        document.body.classList.remove('loading');
        $scope.errors = e;
      });
    }
    modalInstance.result.then(function (newUser) {
      createUser(newUser);
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  }
  $scope.delete = function ($index) {
    document.body.classList.add('loading');
    $http.delete('api/teammember/?id=' + $scope.user.users[$index].id).success(update).error(function (e) {
      alert(e)
    });
  }

});
var editUserCtrl = function ($scope, $modalInstance, user) {
  $scope.user = angular.copy(user);
  $scope.submit = function (form) {
    if (form.$valid) {
      $modalInstance.close($scope.user);
    }
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};
var createUserCtrl = function ($scope, $modalInstance) {
  $scope.user = {};
  $scope.submit = function (form) {
    if (form.$valid) {
      $modalInstance.close($scope.user)
    }
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};