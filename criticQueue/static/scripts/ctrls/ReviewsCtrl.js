criticQueueApp.controller('ReviewsCtrl', function ($scope, $http) {
  $scope.review = {};
  $scope.me = ME;
  $scope.errors;
  document.body.classList.add('loading');
  var update = function(){
    $http.get('api/queue/').success(function (data) {
      $scope.review.reviews = data || [];
      document.body.classList.remove('loading');
    })
      .error(function (err) {
        $scope.error = err;
      });
  }
  update();
  $scope.createReview = function () {
    if ($scope.review.newReview.link) {
      document.body.classList.add('loading');
      $http.post('api/queue/', {url: $scope.review.newReview.link}).success(update).error(function (e) {
        $scope.errors = e;
        document.body.classList.remove('loading');
      })
    }
  }
  $scope.assignToMe = function (id) {
    var $index;
    $scope.review.reviews.forEach(function (el, index) {
      if (el.id === id)
        $index = index;
    });
    document.body.classList.add('loading');
    $http.put('api/queue/?type=assign', { id: $scope.review.reviews[$index].id}).success(update).error(
      function (e) {
        $scope.errors = e;
        document.body.classList.remove('loading');
      });

  }
  $scope.done = function (id) {
    var $index;
    $scope.review.reviews.forEach(function (el, index) {
      if (el.id === id)
        $index = index;
    });
    document.body.classList.add('loading');
    $http.put('api/queue/?type=done', { id: $scope.review.reviews[$index].id}).success(update).error(
      function (e) {
        $scope.errors = e;
        document.body.classList.remove('loading');
      });

  }
})
;