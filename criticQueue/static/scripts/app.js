var criticQueueApp = angular.module('criticQueueApp', ['ngRoute', 'ui.bootstrap', 'ngAnimate']);
criticQueueApp.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.
      when('/users', {
        templateUrl: 'media/scripts/templates/users/users.html',
        controller: 'UsersCtrl'
      }).
      when('/reviews', {
        templateUrl: 'media/scripts/templates/reviews/reviews.html',
        controller: 'ReviewsCtrl'
      }).
      otherwise({
        templateUrl: 'media/scripts/templates/index.html',
        controller: 'MainCtrl'
      });
  }]).config(['$httpProvider', function ($httpProvider) {
  // Intercept POST requests, convert to standard form encoding
  $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
  $httpProvider.defaults.headers.put["Content-Type"] = "application/x-www-form-urlencoded";
  $httpProvider.defaults.transformRequest.unshift(function (data, headersGetter) {
    var key, result = [];
    for (key in data) {
      if (data.hasOwnProperty(key)) {
        result.push(encodeURIComponent(key) + "=" + encodeURIComponent(data[key]));
      }
    }
    return result.join("&");
  });
}]).directive("passwordVerify", function () {
  return {
    require: "ngModel",
    scope: {
      passwordVerify: '='
    },
    link: function (scope, element, attrs, ctrl) {
      scope.$watch(function () {
        var combined;

        if (scope.passwordVerify || ctrl.$viewValue) {
          combined = scope.passwordVerify + '_' + ctrl.$viewValue;
        }
        return combined;
      }, function (value) {
        if (value) {
          ctrl.$parsers.unshift(function (viewValue) {
            var origin = scope.passwordVerify;
            if (origin !== viewValue) {
              ctrl.$setValidity("passwordVerify", false);
              return undefined;
            } else {
              ctrl.$setValidity("passwordVerify", true);
              return viewValue;
            }
          });
        }
      });
    }
  };
}).filter('property', function () {
  function parseString(input) {
    return input.split(".");
  }

  function getValue(element, propertyArray) {

    var value = element;
    if (propertyArray)
      propertyArray.forEach(function (property) {
        if(value && value[property])
          value = value[property];
        else
          value = null;
      });
    return value;
  }

  return function (array, propertyString, target, not) {
    console.log(arguments);
    var properties = parseString(propertyString);
    if (array)
      return array.filter(function (item) {
        var value = getValue(item, properties);
        console.log('val', value);
        if(value) {
          if (!not)
            return value == target;
          else
            return value != target;
        }

      });
  }
});
