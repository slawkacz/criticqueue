# README #

### How do I get set up? ###

* ./python bootstrap.py
* ./bin/buildout
* edit criticQueue.settings
* ./bin/django syncdb (create admin user)
* ./bin/django runserver --settings=criticQueue.development

### Who do I talk to? ###

* Sławek Kaczorowski (skaczorowski@opera.com)